const { i18n } = require("./next-i18next.config");
const nextConfig = {
  images: {
    unoptimized: false,
  },
  reactStrictMode: true,
  swcMinify: true,
  i18n,
  compiler: {
    styledComponents: true,
  },
};

module.exports = nextConfig;
