import Link from "next/link";
import { useRouter } from "next/router";

export default function LocaleSwitcher({ scroller }) {
  const router = useRouter();

  const { locales, locale: activeLocale } = router;

  const otherLocales = locales?.filter((locale) => locale !== activeLocale);

  return (
    <>
      {otherLocales?.map((locale) => {
        const { pathname, query, asPath } = router;
        return (
          <div key={"locale-" + locale}>
            <div className="grid grid-cols-2 gap-0">
              <Link
                href={{ pathname, query }}
                as={asPath}
                locale={locale}
                className={`${
                  locale === "en" ? "pointer-events-none cursor-default" : ""
                }`}
              >
                <div
                  className={`ml-2 my-1 h-8 w-8 rounded-full  pt-1 pb-1.5 font-semibold text-md text-center hover:bg-white  hover:text-gray-600 ${
                    locale === "en"
                      ? scroller > 0
                        ? "text-white bg-gray-600"
                        : "bg-white text-gray-600"
                      : scroller > 0
                      ? "text-dark"
                      : "text-white"
                  } ${scroller}`}
                >
                  ID
                </div>
              </Link>
              <Link
                href={{ pathname, query }}
                as={asPath}
                locale={locale}
                className={`${
                  locale === "id" ? "pointer-events-none cursor-default" : ""
                }`}
              >
                <div
                  className={`my-1 h-8 w-8 rounded-full pt-1 pb-1.5 font-semibold text-md  text-center hover:bg-light hover:text-gray-600 ${
                    locale === "id"
                      ? scroller > 0
                        ? "text-white bg-gray-600"
                        : "bg-white text-gray-600"
                      : scroller > 0
                      ? "text-dark"
                      : "text-white"
                  }`}
                >
                  EN
                </div>
              </Link>
            </div>
          </div>
        );
      })}
    </>
  );
}
