import LogoImage from "next/image";
import Link from "next/link";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import LocaleSwitcher from "../language-switcher";
import { useEffect, useState } from "react";

function Navbar() {
  const [navbar, setNavbar] = useState(false);
  const router = useRouter();
  const { t } = useTranslation("");

  const [clientWindowHeight, setClientWindowHeight] = useState(0);

  const [backgroundTransparacy, setBackgroundTransparacy] = useState(0);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  const handleScroll = () => {
    setClientWindowHeight(window.scrollY);
  };

  useEffect(() => {
    let backgroundTransparacyVar = clientWindowHeight / 600;

    if (backgroundTransparacyVar < 1) {
      let paddingVar = 10 - backgroundTransparacyVar * 20;
      let boxShadowVar = backgroundTransparacyVar * 0.1;
      setBackgroundTransparacy(backgroundTransparacyVar);
    }
  }, [clientWindowHeight]);

  return (
    <>
      <div
        className="grid grid-cols-[repeat(auto-fit,_35%)] max-sm:grid-cols-[repeat(auto-fit,_45%)] max-xl:grid-cols-[repeat(auto-fit,_45%)] max-xl:px-5 m-auto justify-center shadow-sm fixed w-full z-10 text-gray-900  bg-opacity-50 dark:bg-dark dark:text-gray-100 backdrop-filter backdrop-blur-lg dark:bg-opacity-50"
        style={{
          background: `rgba(255, 255, 255, ${backgroundTransparacy})`,
        }}
      >
        <div className="w-full col-span-2">
          {/* Desktop Menu Start*/}
          <div className="grid grid-cols-2 gap-3 py-3 max-xl:grid-cols-4 max-xl:py-2">
            <div className="grid grid-cols-4 gap-3 max-xl:col-span-3">
              <div className="w-32 max-xl:w-32">
                <Link href="/">
                  <LogoImage
                    src={`${
                      backgroundTransparacy > 0
                        ? "/sorcha-logo.png"
                        : "/sorcha-logo-white.png"
                    }`}
                    width="150"
                    height="50"
                    alt={"Logo Sorcha"}
                    className="w-auto h-auto"
                    priority
                    unoptimized
                  />
                </Link>
              </div>
              <div className="md:hidden">
                <button
                  className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                  onClick={() => setNavbar(!navbar)}
                >
                  {navbar ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6 text-white"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6 text-white"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4 6h16M4 12h16M4 18h16"
                      />
                    </svg>
                  )}
                </button>
              </div>
              <div className="max-md:hidden col-span-3">
                <div className="flex">
                  <div className="text-sm w-36 text-center justify-items-center pt-2 max-xl:pt-3">
                    <Link
                      href="/about-us"
                      className={`pb-2 font-medium hover:border-b-2 hover:border-primary hover:text-primary hover:transition-colors before:text-dark after:text-primary focus:border-b-2 focus:border-primary focus:text-primary 
                      ${
                        backgroundTransparacy > 0
                          ? router?.pathname === "/about-us"
                            ? "border-b-2 border-primary text-primary"
                            : "text-dark"
                          : router?.pathname === "/about-us"
                          ? "border-b-2 border-primary text-primary"
                          : "text-white"
                      }
                      `}
                    >
                      {t("menu.About")}
                    </Link>
                  </div>
                  <div className="text-sm w-28 text-center justify-items-center pt-2  max-xl:pt-3">
                    <Link
                      href="/services"
                      className={`pb-2 font-medium hover:border-b-2 hover:border-primary hover:text-primary hover:transition-colors before:text-dark after:text-primary focus:border-b-2 focus:border-primary focus:text-primary ${
                        backgroundTransparacy > 0
                          ? router?.pathname === "/services"
                            ? "border-b-2 border-primary text-primary"
                            : "text-dark"
                          : router?.pathname === "/services"
                          ? "border-b-2 border-primary text-primary"
                          : "text-white"
                      }
                      `}
                    >
                      {t("menu.Services")}
                    </Link>
                  </div>
                  <div className="text-sm w-28 text-center justify-items-center pt-2 max-xl:pt-3">
                    <Link
                      href="/portfolio"
                      className={`pb-2 font-medium hover:border-b-2 hover:border-primary hover:text-primary hover:transition-colors before:text-dark after:text-primary focus:border-b-2 focus:border-primary focus:text-primary ${
                        backgroundTransparacy > 0
                          ? router?.pathname === "/portfolio"
                            ? "border-b-2 border-primary text-primary"
                            : "text-dark"
                          : router?.pathname === "/portfolio"
                          ? "border-b-2 border-primary text-primary"
                          : "text-white"
                      }
                      `}
                    >
                      {t("menu.Portfolio")}
                    </Link>
                  </div>
                  <div className="text-sm w-20 text-center justify-items-center pt-2 max-xl:pt-3">
                    <Link
                      href="/blog"
                      className={`pb-2 font-medium hover:border-b-2 hover:border-primary hover:text-primary hover:transition-colors before:text-dark after:text-primary focus:border-b-2 focus:border-primary focus:text-primary ${
                        backgroundTransparacy > 0
                          ? router?.pathname === "/blog"
                            ? "border-b-2 border-primary text-primary"
                            : "text-dark"
                          : router?.pathname === "/blog"
                          ? "border-b-2 border-primary text-primary"
                          : "text-white"
                      }
                      `}
                    >
                      {t("menu.Blog")}
                    </Link>
                  </div>
                  <div className="text-sm w-24 text-center justify-items-center pt-2 max-xl:pt-3">
                    <Link
                      href="/contact-us"
                      className={`pb-2 font-medium hover:border-b-2 hover:border-primary hover:text-primary hover:transition-colors before:text-dark after:text-primary focus:border-b-2 focus:border-primary focus:text-primary ${
                        backgroundTransparacy > 0
                          ? router?.pathname === "/contact-us"
                            ? "border-b-2 border-primary text-primary"
                            : "text-dark"
                          : router?.pathname === "/contact-us"
                          ? "border-b-2 border-primary text-primary"
                          : "text-white"
                      }
                      `}
                    >
                      {t("menu.Contact")}
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-3 max-xl:grid-cols-1 gap-4 max-xl:gap-0">
              <div></div>
              <div className="grid grid-cols-6 gap-4 max-xl:hidden">
                <div>
                  <Link href="https://www.facebook.com/" target={"_blank"}>
                    <svg
                      className={`h-5 w-7 mt-2.5 hover:text-blue-700 ${
                        backgroundTransparacy > 0 ? "text-dark" : "text-white"
                      }`}
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      strokeWidth="2"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" />
                      <path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3" />
                    </svg>
                  </Link>
                </div>
                <div>
                  <Link href="https://www.instagram.com/" target={"_blank"}>
                    <svg
                      className={`h-5 w-7 mt-2.5 hover:text-pink-600 ${
                        backgroundTransparacy > 0 ? "text-dark" : "text-white"
                      }`}
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      strokeWidth="2"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" />
                      <rect x="4" y="4" width="16" height="16" rx="4" />
                      <circle cx="12" cy="12" r="3" />
                      <line x1="16.5" y1="7.5" x2="16.5" y2="7.501" />
                    </svg>
                  </Link>
                </div>
                <div>
                  <Link href="https://www.twitter.com/" target={"_blank"}>
                    <svg
                      className={`h-5 w-7 mt-2.5 hover:text-teal-400 ${
                        backgroundTransparacy > 0 ? "text-dark" : "text-white"
                      }`}
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      strokeWidth="2"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" />
                      <path d="M22 4.01c-1 .49-1.98.689-3 .99-1.121-1.265-2.783-1.335-4.38-.737S11.977 6.323 12 8v1c-3.245.083-6.135-1.395-8-4 0 0-4.182 7.433 4 11-1.872 1.247-3.739 2.088-6 2 3.308 1.803 6.913 2.423 10.034 1.517 3.58-1.04 6.522-3.723 7.651-7.742a13.84 13.84 0 0 0 .497 -3.753C20.18 7.773 21.692 5.25 22 4.009z" />
                    </svg>
                  </Link>
                </div>
                <div className="col-span-3 border-l border-l-light">
                  <LocaleSwitcher scroller={`${backgroundTransparacy}`} />
                </div>
              </div>
              <div>
                <Link
                  href="/consultations"
                  className={`w-full text-center flex rounded-full border-2 px-6 pt-2 pb-[6px] text-xs font-medium uppercase leading-normal text-neutral-50 transition duration-150 ease-in-out hover:border-neutral-100 hover:bg-neutral-500 hover:bg-opacity-10 hover:text-neutral-100 focus:border-neutral-100 focus:text-neutral-100 focus:outline-none focus:ring-0 active:border-neutral-200 active:text-neutral-200 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10
                  ${
                    backgroundTransparacy > 0
                      ? "text-primary border-primary"
                      : "text-white border-white"
                  }
                  `}
                >
                  <div className="w-4 text-center">
                    <svg
                      className={`h-5 w-5 ${
                        backgroundTransparacy > 0
                          ? "text-primary"
                          : "text-white"
                      }`}
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      strokeWidth="2"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" />
                      <rect x="4" y="13" rx="2" width="4" height="6" />
                      <rect x="16" y="13" rx="2" width="4" height="6" />
                      <path d="M4 15v-3a8 8 0 0 1 16 0v3" />
                      <path d="M18 19a6 3 0 0 1 -6 3" />
                    </svg>
                  </div>
                  <div className="w-full">{t("menu.FreeConsultations")}</div>
                </Link>
              </div>
            </div>
          </div>
          {/* Desktop Menu End*/}
        </div>
      </div>
      <script></script>
    </>
  );
}

export default Navbar;
